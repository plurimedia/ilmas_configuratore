import Vue from 'vue';
// eslint-disable-next-line import/no-extraneous-dependencies
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import Axios from 'axios';
import _ from 'lodash';
import VueI18n from 'vue-i18n';
import App from './App.vue';
import router from './router';

// eslint-disable-next-line import/no-extraneous-dependencies
import 'bootstrap/dist/css/bootstrap.css';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'bootstrap-vue/dist/bootstrap-vue.css';

// css custom for Alfa Laval
import '@/assets/css/typography.css';
import '@/assets/css/base.css';
import '@/assets/css/col.css';
import '@/assets/css/cards.css';
import '@/assets/css/header.css';
import '@/assets/css/layout.css';
import '@/assets/css/stepper.css';
import '@/assets/css/form.css';

import '@fortawesome/fontawesome-free/css/all.css';
//import '@fortawesome/fontawesome-free/js/all.js';
import store from './store';
import messages from './locale/messages';

Vue.use(VueI18n);

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

Vue.config.productionTip = false;

Axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;
Vue.prototype.$http = Axios;
/* eslint-disable no-underscore-dangle */
Vue.prototype.$_ = _;

//store.commit('setLanguage', 'it');

const i18n = new VueI18n({
  locale: store.getters.getLanguage,
  messages
});

Axios.get('/api/v1/estro/id-profili')
  .then((result) => {
    if (result.data) store.commit('setIdProfili', result.data);    
  
    Axios.get('/api/v1/estro/id-fonti')
      .then((result2) => {
        store.commit('setIdFonti', result2.data);

        new Vue({
          router,
          store,
          render: (h) => h(App),
          i18n
        }).$mount('#app');
      })
      .catch((err) => {
        console.log('error on fonti id', err);
      });

  })
  .catch((err) => {
    console.log('error on get profili id', err);
  });
