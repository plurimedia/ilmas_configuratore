import Vue from 'vue';
import VueRouter from 'vue-router';
import Applicazione from '../views/Applicazione.vue';
import store from '../store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: 'step1'
  },
  {
    path: '/en',
    beforeEnter: (to, from, next) => {
      store.commit('setLanguage', 'en');
      next({ name: 'Applicazione' });
    },
  },
  {
    path: '/step1',
    name: 'Applicazione',
    component: Applicazione,
  },
  {
    path: '/step2',
    name: 'Profilo',
    component: () => import('../views/Profilo.vue'),
  },
  {
    path: '/step3',
    name: 'Diffusore',
    component: () => import('../views/Diffusore.vue'),
  },
  {
    path: '/step4',
    name: 'Led',
    component: () => import('../views/Led.vue'),
  },
  {
    path: '/step5',
    name: 'Lunghezza',
    component: () => import('../views/Lunghezza.vue'),
  },
  {
    path: '/step6',
    name: 'Finitura',
    component: () => import('../views/Finiture.vue'),
  },
  {
    path: '/riepilogo',
    name: 'Riepilogo',
    component: () => import('../views/Riepilogo.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
