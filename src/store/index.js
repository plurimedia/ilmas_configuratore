/* eslint-disable no-nested-ternary */
/* eslint-disable arrow-body-style */
/* eslint-disable arrow-parens */
/* eslint-disable no-underscore-dangle */
import Vue from 'vue';
import Vuex from 'vuex';
import _ from 'lodash';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    language: null,
    currentGlobal: null,
    currentGlobalOther: null,
    currentRoute: null,
    currentStep: null,
    idprofili: null,
    idfonti: null,
    applicazione: {},
    profilo: {},
    diffusore: {},
    led: {},
    fonte: {},
    lunghezza: {},
    finitura: {},
    riepilogo: {},
    codice: null,
    finituraProfiloPrincipale: null
  },
  mutations: {
    setLanguage(state, value){
      sessionStorage.setItem('language', value);
      state.language = value;
    },
    setCurrentGlobal(state, value){
      sessionStorage.setItem('currentGlobal', value);
      state.currentGlobal = value;
    },
    setCurrentGlobalOther(state, value){
      sessionStorage.setItem('currentGlobalOther', value);
      state.currentGlobalOther = value;
    },
    setCurrentRoute(state, value){
      sessionStorage.setItem('currentRoute', value);
      state.currentRoute = value;
    },
    setCurrentStep(state, value){
      sessionStorage.setItem('currentStep', value);
      state.currentStep = value;
    },
    setIdProfili(state, value){
      sessionStorage.setItem('idprofili', value);
      state.idprofili = value;
    },
    setIdFonti(state, value){
      sessionStorage.setItem('idfonti', value);
      state.idfonti = value;
    },
    setApplicazione(state, value){
      if (value){
        sessionStorage.setItem('applicazione', value);
        state.applicazione = value;
      } else {
        sessionStorage.removeItem('applicazione');
        state.applicazione = {};
      }     
    },
    setProfilo(state, value){
      if (value){
        sessionStorage.setItem('profilo', value);
        state.profilo = value;
      } else {
        sessionStorage.removeItem('profilo');
        state.profilo = {};
      }    
    },
    setDiffusore(state, value){
      if (value){
        sessionStorage.setItem('diffusore', value);
        state.diffusore = value;
      } else {
        sessionStorage.removeItem('diffusore');
        state.diffusore = {};
      }    
    },
    setLed(state, value){
      if (value){
        sessionStorage.setItem('led', value);
        state.led = value;
      } else {
        sessionStorage.removeItem('led');
        state.led = {};
      } 
    },
    setFonte(state, value){
      if (value){
        sessionStorage.setItem('fonte', value);
        state.fonte = value;
      } else {
        sessionStorage.removeItem('fonte');
        state.fonte = {};
      } 
    },
    setLunghezza(state, value){
      if (value){
        sessionStorage.setItem('lunghezza', value);
        state.lunghezza = value;
      } else {
        sessionStorage.removeItem('lunghezza');
        state.lunghezza = {};
      } 
    },
    setFinitura(state, value){
      if (value){
        sessionStorage.setItem('finitura', value);
        state.finitura = value;
      } else {
        sessionStorage.removeItem('finitura');
        state.finitura = {};
      }
    },
    setRiepilogo(state, value){
      if (value){
        sessionStorage.setItem('riepilogo', value);
        state.riepilogo = value;
      } else {
        sessionStorage.removeItem('riepilogo');
        state.riepilogo = {};
      }
    },
    setCodice(state, value){
      if (value){
        sessionStorage.setItem('codice', value);
        state.codice = value;
      } else {
        sessionStorage.removeItem('codice');
        state.codice = {};
      } 
    },
    setFinituraProfiloPrincipale(state, value){
      if (value){
        sessionStorage.setItem('finituraProfiloPrincipale', value);
        state.finituraProfiloPrincipale = value;
      } else {
        sessionStorage.removeItem('finituraProfiloPrincipale');
        state.finituraProfiloPrincipale = null;
      } 
    },
  },
  actions: {
  },
  modules: {
  },
  getters: {
    getLanguage: state => { return state.language ? state.language : (sessionStorage.getItem('language') ? sessionStorage.getItem('language') : 'it'); },
    getCurrentGlobal: state => { return state.currentGlobal ? state.currentGlobal : (sessionStorage.getItem('currentGlobal') ? sessionStorage.getItem('currentGlobal') : 'principal'); },
    getCurrentRoute: state => { return state.currentRoute ? state.currentRoute : (sessionStorage.getItem('currentRoute') ? sessionStorage.getItem('currentRoute') : null); },
    getCurrentStep: state => { return state.currentStep ? state.currentStep : (sessionStorage.getItem('currentStep') ? sessionStorage.getItem('currentStep') : 1); },
    getIdProfili: state => { return state.idprofili ? state.idprofili : (sessionStorage.getItem('idprofili') ? sessionStorage.getItem('idprofili') : null); },
    getIdFonti: state => { return state.idfonti ? state.idfonti : (sessionStorage.getItem('idfonti') ? sessionStorage.getItem('idfonti') : null); },
    getApplicazione: state => { return !_.isEmpty(state.applicazione) ? JSON.parse(state.applicazione) : (sessionStorage.getItem('applicazione') ? JSON.parse(sessionStorage.getItem('applicazione')) : {}); },
    getProfilo: state => { return !_.isEmpty(state.profilo) ? JSON.parse(state.profilo) : (sessionStorage.getItem('profilo') ? JSON.parse(sessionStorage.getItem('profilo')) : {}); },
    getDiffusore: state => { return !_.isEmpty(state.diffusore) ? JSON.parse(state.diffusore) : (sessionStorage.getItem('diffusore') ? JSON.parse(sessionStorage.getItem('diffusore')) : {}); },
    getLed: state => { return !_.isEmpty(state.led) ? JSON.parse(state.led) : (sessionStorage.getItem('led') ? JSON.parse(sessionStorage.getItem('led')) : {}); },
    getFonte: state => { return !_.isEmpty(state.fonte) ? JSON.parse(state.fonte) : (sessionStorage.getItem('fonte') ? JSON.parse(sessionStorage.getItem('fonte')) : {}); },
    getLunghezza: state => { return !_.isEmpty(state.lunghezza) ? JSON.parse(state.lunghezza) : (sessionStorage.getItem('lunghezza') ? JSON.parse(sessionStorage.getItem('lunghezza')) : {}); },
    getCodice: state => { return state.codice ? state.codice : (sessionStorage.getItem('codice') ? sessionStorage.getItem('codice') : ''); },
    getFinitura: state => { return !_.isEmpty(state.finitura) ? JSON.parse(state.finitura) : (sessionStorage.getItem('finitura') ? JSON.parse(sessionStorage.getItem('finitura')) : {}); },
    getFinituraProfiloPrincipale: state => { return state.finituraProfiloPrincipale ? JSON.parse(state.finituraProfiloPrincipale) : (sessionStorage.getItem('finituraProfiloPrincipale') ? JSON.parse(sessionStorage.getItem('finituraProfiloPrincipale')) : null); },
    getRiepilogo: state => { return !_.isEmpty(state.riepilogo) ? JSON.parse(state.riepilogo) : (sessionStorage.getItem('riepilogo') ? JSON.parse(sessionStorage.getItem('riepilogo')) : {}); }, 
  }
});
